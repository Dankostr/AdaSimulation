package body Boss is

   protected body Generator is
      procedure Generate_Random_Number(Result: out Float) is
         Rnd: Float:= Ada.Numerics.Float_Random.Random(Gen);
      begin
         Result:= 100.0 * Rnd;
      end  Generate_Random_Number;
      function Generate_Operation return Jobs.Op is
         operations : Char_Arr :=('+','*');
         begin
            return operations(operations'First+Natural(
                              Ada.Numerics.Float_Random.Random(Gen)*Float(operations'Last)));
      end Generate_Operation;
      function Generate_Delay return Standard.Duration is
      begin
         return Standard.Duration(Float(Ada.Numerics.Float_Random.Random(Gen))*variables.BossSleep);
      end Generate_Delay;
   end Generator;
   task body t is
      number1: Float;
      number2: Float;
      operation : Jobs.Op;
      job : Jobs.Job;
      JobListTask : JobList.Job_List_Pointer;
   begin
      accept Start (jobT : in JobList.Job_List_Pointer) do
         JobListTask:=jobT;
      end Start;
      loop
      Generator.Generate_Random_Number(number1);
      Generator.Generate_Random_Number(number2);
      operation:=Generator.Generate_Operation;
      job:=(X => number1,Y=>number2,O=>operation,Result=>0.0);
      JobListTask.Insert(job);
         if variables.Is_Silent = false then
            Put_Line("Boss created job:" & Float'Image(number1) & Jobs.Op'Image(operation) & Float'Image(number2));
         end if;

         delay(Generator.Generate_Delay);
      end loop;

   end t;
end Boss;
