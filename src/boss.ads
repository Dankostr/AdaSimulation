with variables;use variables;
with Ada.Text_IO;use Ada.Text_IO;
with Ada.Numerics.Float_Random;use Ada.Numerics.Float_Random;
with Jobs;use Jobs;
with JobList;use JobList;
package Boss is
   task type t is
      entry Start(jobT: in JobList.Job_List_Pointer);
      end t;

   type Char_Arr is array (Natural range <>) of Jobs.Op;

   protected Generator is
      procedure Generate_Random_Number(Result: out Float);
      function Generate_Operation return Jobs.Op;
      function Generate_Delay return Standard.Duration;
      private
      Gen : Ada.Numerics.Float_Random.Generator;

   end Generator;



end Boss;
