package body ClientPackage is
   task body Client is
      MagazineTask: MagazinePackage.Magazine_Pointer;
      result : Float;
      Client_Id : Integer;
   begin
         accept Start(magazineT : MagazinePackage.Magazine_Pointer; cid: Integer) do
            Client_Id:=cid;
         MagazineTask:=magazineT;
      end Start;
      loop
         MagazineTask.Retrive(result);
         if variables.Is_Silent=false then
            Put_Line("Client with id" & Integer'Image(Client_Id) & " bought item with result: " &Float'Image(result));
         end if;
         delay(variables.ClientSleep);
      end loop;
      end Client;
end ClientPackage;
