with MagazinePackage;use MagazinePackage;
with Ada.Text_IO;use Ada.Text_IO;
with variables;use variables;
package ClientPackage is
   task type Client is
      entry Start(magazineT: MagazinePackage.Magazine_Pointer ; cid : in  Integer);
   end Client;
   type Client_Pointer is access Client;
end ClientPackage;
