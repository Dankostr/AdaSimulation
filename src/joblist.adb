package body JobList is

   procedure Print_Elem(j: in Jobs.Job) is
   begin
      Put_Line("Job : "&Float'Image(j.X) & " " &Jobs.Op'Image(j.O) & " "&Float'Image(j.Y));
      end Print_Elem;


   procedure Print_List(elem: in JobContrainter.Cursor) is
   begin
      Print_Elem(JobContrainter.Element(elem));
      end Print_List;

   task body Job_List is
      jobsarray: List;
   begin
      loop
         select
            when jobsarray.Length<variables.JobListSize =>
            accept Insert(j: in Jobs.Job) do
               jobsarray.Append(j);
            end Insert;
         or
            when jobsarray.Length>0 =>
            accept Retrive(j : out Jobs.Job) do
               j:=jobsarray.First_Element;
               jobsarray.Delete_First;
               end Retrive;
         or
            accept Info do
               Put_Line(Ada.Containers.Count_Type'Image(jobsarray.Length));
               jobsarray.Iterate(Print_List'Access);
            end Info;
         end select;
      end loop;
   end Job_List;
end JobList;
