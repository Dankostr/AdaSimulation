with Jobs;use Jobs;
with Ada.Containers.Doubly_Linked_Lists;use Ada.Containers;
with variables;use variables;
with Ada.Text_IO;use Ada.Text_IO;

package JobList is
   package JobContrainter is new Doubly_Linked_Lists(Jobs.Job);use JobContrainter;
    procedure Print_List(elem: in JobContrainter.Cursor);
        procedure Print_Elem(j: in Jobs.Job);
   task type Job_List is
	entry Insert(j : in Jobs.Job);
      entry Retrive(j: out Jobs.Job);
      entry Info;
  end Job_List;
 type Job_List_Pointer is access all Job_List;
end JobList;
