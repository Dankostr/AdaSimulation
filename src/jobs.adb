package body Jobs is
   function  Do_Job(j: in Job) return Float is
   begin
   return ( case j.O is
              when '+' => j.X+j.Y,
              when '*' => j.X*j.Y);
   end Do_Job;
end Jobs;
