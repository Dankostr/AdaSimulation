with Ada.Text_IO; use Ada.Text_IO;
package body machine is


   protected body Generator is
      function Generate_Float return Float is
         Rnd: Float:= Ada.Numerics.Float_Random.Random(Gen);
      begin
         return Rnd;
      end  Generate_Float;
   end Generator;

   task body Machine is
      Broken: Boolean:=False;
      f: Float;
   begin
      loop
         select
            accept Repair  do
               Broken:=False;
            end Repair;
            accept FreeMachine  do
               null;
            end FreeMachine;
         or

            accept Insert (j : in out Jobs.Job) do
               if Broken=False then
                  f:=Generator.Generate_Float;
                  If( f >0.8) then
                     Broken:=True;
                  end if;
               end if;
               if Broken=False then
                  j.Result:=Jobs.Do_Job(j);
               else
                  j.Result:=0.0;
               end if;

               delay(variables.MachineWorkTime);
            end Insert;

         end select;
      end loop;
   end Machine;

end machine;
