with Jobs;use Jobs;
with variables;use variables;
with Ada.Numerics.Float_Random;
package machine is
   task type Machine is 
      entry Insert(j :in out Jobs.Job);
      entry Repair;
      entry FreeMachine;
   end Machine;
 type Machine_Pointer is access all Machine;

    protected Generator is
      function Generate_Float return Float;
      private
      Gen : Ada.Numerics.Float_Random.Generator;

   end Generator;
   
   
end machine;
