with Ada.Containers.Doubly_Linked_Lists; use Ada.Containers;
with Ada.Text_IO; use Ada.Text_IO;
package body machineroom is
   task body MachineRoom is

      SumMachines : Array(1..variables.SumMachines) of Machine_Pointer;
      MultiplyMachines :Array(1..variables.MultiplyMachines) of Machine_Pointer;
      IndexSum : Natural := 1;
      IndexMultiply : Natural :=1;
   begin
      for i in 1 .. variables.SumMachines loop
         SumMachines(i):=(new machine.Machine);
      end loop;
      for i in 1 .. variables.MultiplyMachines loop
         MultiplyMachines(i):=(new machine.Machine);
      end loop;
      loop
         select
            accept GetMachine (j : in Jobs.Job; m : out machine.Machine_Pointer;id: out Natural) do

               if j.O = '+' then
                  m:=SumMachines(IndexSum);
                  id:=IndexSum;
                  IndexSum:=IndexSum+1;
                  IndexSum:=IndexSum mod (variables.SumMachines+1);
                  if(IndexSum=0) then
                     IndexSum:=1;
                  end if;

               elsif j.O = '*' then
                  m:=MultiplyMachines(IndexMultiply);
                  id:=IndexMultiply;
                  IndexMultiply:=IndexMultiply+1;
                  IndexMultiply:=IndexMultiply mod (variables.MultiplyMachines+1);
                  if(IndexMultiply=0) then
                        IndexMultiply:=1;
                     end if;
               end if;
            end GetMachine;
         or
            accept RepairMachine (id : in Natural; o : in Jobs.Op; m : out machine.Machine_Pointer) do
               if  o='+'  then
                  m:=SumMachines(id);
               elsif o='*' then
                  m:=MultiplyMachines(id);
                  end if;
            end RepairMachine;
         end select;
      end loop;

   end MachineRoom;


end machineroom;
