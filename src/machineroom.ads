with machine;use machine;
with Ada.Containers.Doubly_Linked_Lists; use Ada.Containers;
with variables;use variables;
with Jobs;use Jobs;
package machineroom is
   
   task type MachineRoom is 
      entry GetMachine(j : in Jobs.Job;m : out machine.Machine_Pointer;id : out Natural);
      entry RepairMachine(id : in Natural; o : in Jobs.Op; m : out machine.Machine_Pointer);
   end MachineRoom;
   type MachineRoom_Pointer is access all MachineRoom;
  
     

end machineroom;
