package body MagazinePackage is
   task body  Magazine is
   magazin: List;

      begin
      loop
         select
            when magazin.Length<variables.MagazineSize =>
            accept Insert(j: in Float) do
               magazin.Append(j);
               end Insert;
            or
            when magazin.Length>0 =>
            accept Retrive(j : out Float) do
               j:=magazin.First_Element;
               magazin.Delete_First;
               end Retrive;
         or
            accept Info  do
               Put_Line(Ada.Containers.Count_Type'Image(magazin.Length));
            end Info;
         end select;
      end loop;
   end Magazine;

end MagazinePackage;
