with Ada.Containers.Doubly_Linked_Lists;use Ada.Containers;
with variables;use variables;
with Ada.Text_IO;use Ada.Text_IO;
package MagazinePackage is
      package MagazineContrainer is new Doubly_Linked_Lists(Float);use MagazineContrainer;

	task type Magazine is
	entry Insert(j : in Float);
      entry Retrive(j: out Float);
      entry Info;
   end Magazine;
   type Magazine_Pointer is access Magazine;
end MagazinePackage;
