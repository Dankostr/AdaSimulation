with Ada.Text_IO; use Ada.Text_IO;
with Boss;use Boss;
with JobList;Use JobList;
with MagazinePackage;use MagazinePackage;
with WorkerPackage;use WorkerPackage;
with ClientPackage;use ClientPackage;
with variables;use variables;
with MenuPackage;use MenuPackage;
with Ada.Command_Line;use Ada.Command_Line;
with machine;use machine;
with machineroom;use machineroom;
with ServicePackage;
procedure Main is
   type Array_Of_Workers is array (1..variables.NoOfWorkers) of WorkerPackage.Worker;
   type Array_Of_Clients is array(1..variables.NoOfClients) of ClientPackage.Client;
   bossTask: Boss.t;
   JobListTask: JobList.Job_List_Pointer:= new JobList.Job_List;
   MachineRoomTask : machineroom.MachineRoom_Pointer := new machineroom.MachineRoom;
   WorkerTasks: Array_Of_Workers;
   MagazineTask: MagazinePackage.Magazine_Pointer:= new MagazinePackage.Magazine;
   ClientTasks :Array_Of_Clients;
   MenuTask: MenuPackage.Menu;
   servisants : ServicePackage.Array_of_Service_Workers;
   serv : ServicePackage.Service_Pointer;

begin
   if Ada.Command_Line.Argument_Count>0 then
      if Ada.Command_Line.Argument(1)="-s" then
         variables.Is_Silent:=True;
         end if;
   end if;


   bossTask.Start(jobT => JobListTask);

   serv := new ServicePackage.Service;

   for I in 1..variables.Fixers loop
     servisants(I):=new ServicePackage.ServiceWorker;
     servisants(I).Start(serviceTask => serv);
   end loop;

   serv.Start(machineroomT => MachineRoomTask,
              serviceW     =>servisants );


   for I in Array_Of_Workers'First..Array_Of_Workers'Last loop
      WorkerTasks(I).Start(jobT => JobListTask,magazineT => MagazineTask,id=>I,machineT=>MachineRoomTask,s => serv);
   end loop;

   for I in Array_Of_Clients'First..Array_Of_Clients'Last loop
      ClientTasks(I).Start(magazineT => MagazineTask,cid => I);
   end loop;

   if variables.Is_Silent=True then
   MenuTask.Start(jobT      => JobListTask,
                  magazineT => MagazineTask );
   end if;
end Main;
