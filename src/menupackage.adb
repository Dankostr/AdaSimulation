package body MenuPackage is
   task body Menu is
      JobListTask : JobList.Job_List_Pointer;
      MagazineTask: MagazinePackage.Magazine_Pointer;
      number: Natural;
   begin
       accept Start (jobT : in JobList.Job_List_Pointer; magazineT : in MagazinePackage.Magazine_Pointer) do
            JobListTask:=jobT;
         MagazineTask:=magazineT;
         end Start;
      loop
         Put_Line("Menu");
         Put_Line("1.Jobs to do");
         Put_Line("2.Items in magazine");
         Put_Line("3.Types of Workers");
         Put_Line("4.How many jobs worker did");
         Get(number);
         case number is
            when 1 =>
               JobListTask.Info;
            when 2 =>
               MagazineTask.Info;
            when 3=>
               for i in LazineesArray'First..LazineesArray'Last loop
                  if LazineesArray(i)=True then
                     Put_Line("Worker" & Integer'Image(i) & "is lazy");
                  else
                     Put_Line("Worker" & Integer'Image(i) & "is not lazy");
                  end if;

               end loop;
            when 4=>
               for i in CountArray'First..CountArray'Last loop
                Put_Line("Worker "& Integer'Image(i) &"did " & Integer'Image(CountArray(i)) & " jobs");
               end loop;
            when others=>
               null;
         end case;
         end loop;
    end Menu;
end MenuPackage;
