with Ada.Text_IO;use Ada.Text_IO;
with JobList; Use JobList;
with MagazinePackage;use MagazinePackage;
With Ada.Integer_Text_IO;use Ada.Integer_Text_IO;
with variables;use variables;
package MenuPackage is
   LazineesArray : array(1..variables.NoOfWorkers) of Boolean:=(others=> False);
   CountArray : array(1..variables.NoOfWorkers) of Integer:=(others=>0);
   task type Menu is
      entry Start(jobT: in JobList.Job_List_Pointer ; magazineT: in MagazinePackage.Magazine_Pointer  );
   end Menu;
end MenuPackage;
