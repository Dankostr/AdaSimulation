with Ada.Task_Identification; use Ada.Task_Identification;
with Ada.Task_Attributes;
with machine; use machine;
package body ServicePackage is

   task body Service is
      
      type Array_Of_Sum_Machines_States is array (1..variables.SumMachines) of Boolean;
      type Array_Of_Multiply_Machines_States is array (1..variables.MultiplyMachines) of Boolean;
      --   type Array_of_Service_Workers is array (1..variables.Fixers) of ServiceWorkerPackage.Pointer;
      MachineRoomTask : machineroom.MachineRoom_Pointer;
      StatesSumMachines: Array_Of_Sum_Machines_States:=(others=>True);
      StatesMultiplyMachines: Array_Of_Multiply_Machines_States:=(others=>True);
      ServiceWorkers : Array_of_Service_Workers;
      Index : Integer:=1;
      This : Service_Pointer:=Service'Unchecked_Access;
      m : machine.Machine_Pointer;
      Queue: List;
   begin
      accept Start (machineroomT : in machineroom.MachineRoom_Pointer; serviceW : in Array_of_Service_Workers) do
         MachineRoomTask:=machineroomT;

         for I in 1..variables.Fixers loop
            ServiceWorkers(I):=serviceW(I);
         end loop;
      end Start;
      loop 
         select
            accept HelpDesk (id : in Natural; operation : in Jobs.Op) do
               if (operation='+' and then StatesSumMachines(id)=False) or (operation='*' and then StatesMultiplyMachines(id)=False) then
                                    if variables.Is_Silent = false then

                     Put_Line("HelpDesk: Ignoring report of machine id: "&Integer'Image(id) &" of type:"&Jobs.Op'Image(operation)&" . Machine already under repairment");
                     end if;
               else
                  if operation='+' then
                     StatesSumMachines(id):=False;
                  elsif operation='*' then
                     StatesMultiplyMachines(id):=False;
                  end if;
                  MachineRoomTask.RepairMachine(id,operation,m);
                                    if variables.Is_Silent = false then

                     Put_Line("HelpDesk: Accepting report of machine id: "&Integer'Image(id) &" of type:"&Jobs.Op'Image(operation)&" . Machine is going for maintenance");
                     end if;
                  Queue.Append((id,operation,m));
                 
               end if;
                  
            end HelpDesk;
         or
            accept RepairedEntry (id : in Natural; operation : in Jobs.Op) do
               if operation = '+' then
                  StatesSumMachines(id):=True;
                                    if variables.Is_Silent = false then
                     
                     Put_Line("HelpDesk:Repaired machine type + of id"&Natural'Image(id));
                     end if;
               elsif operation ='*' then
                  StatesMultiplyMachines(id):=True;
                                    if variables.Is_Silent = false then

                     Put_Line("HelpDesk:Repaired machine type * of id"&Natural'Image(id));
                     end if;
               end if; 
            end RepairedEntry;
         or 
            when Queue.Length>0 =>
               accept ToRepair (Rec : out Info) do
                  Rec:=Queue.First_Element;
                  Queue.Delete_First;
               end ToRepair;  
         end select;
      end loop;
         
   end Service;
   
   
   task body ServiceWorker is
      serv : ServicePackage.Service_Pointer;
      Rec : Info;
   begin
      accept Start(serviceTask : ServicePackage.Service_Pointer)  do

         serv:=serviceTask;
      end Start;
      loop      
         serv.ToRepair(Rec);
         delay(variables.FixerTimeToArrive);
         Rec.M.Repair;
         serv.RepairedEntry(id        => Rec.I,
                            operation => Rec.O);
         Rec.M.FreeMachine;
      end loop;
   end ServiceWorker;

end ServicePackage;
