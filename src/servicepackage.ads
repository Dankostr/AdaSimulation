with variables;use variables;
with machineroom;use machineroom;
with Jobs;use Jobs;
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Task_Identification;use Ada.Task_Identification;
with machine; use machine;
with Ada.Containers.Doubly_Linked_Lists;use Ada.Containers;

package ServicePackage is

   type ServiceWorker;
   type Pointer is access all ServiceWorker;
   type Service;
   type Service_Pointer is access all Service;
   type Info is
      record
         I : Natural;
         O : Jobs.Op;
         M : machine.Machine_Pointer;
      end record;

      package ReapirInfoPackage is new Doubly_Linked_Lists(Info);use ReapirInfoPackage;


  type Array_of_Service_Workers is array (1..variables.Fixers) of  Pointer;
  task type Service is

      entry Start(machineroomT : in machineroom.MachineRoom_Pointer;serviceW : in Array_of_Service_Workers);
      entry HelpDesk(id: Natural ;operation :Jobs.Op);
      entry ToRepair(Rec : out Info);
      entry RepairedEntry(id : in Natural; operation : in Jobs.Op);
   end Service;


    task type ServiceWorker is
      entry Start(serviceTask : ServicePackage.Service_Pointer);
   end ServiceWorker;


end ServicePackage;
