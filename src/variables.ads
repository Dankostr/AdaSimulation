with Ada.Containers;use Ada.Containers;
package variables is
   Is_Silent : Boolean := False;
   NoOfWorkers: Natural := 10;
   NoOfClients: Natural:=3;
   WorkerSleep:Duration:=1.0;
   ClientSleep:Duration:=2.0;
   MagazineSize:Ada.Containers.Count_Type :=100;
   JobListSize:Ada.Containers.Count_Type:=100;
   BossSleep: Float:=0.1;
   MachineWorkTime:Duration:=1.0;
   SumMachines: Integer:=1;
   MultiplyMachines : Integer:=1;
   WorkerWait:Duration := 2.0;
   Fixers: Natural :=2;
   FixerTimeToArrive : Duration := 0.0;
end variables;
