package body WorkerPackage is


   protected body Generator is

      function Generate_Laziness return Boolean is
         operations : BooleanArr :=(False,True);
      begin
         Ada.Numerics.Float_Random.Reset(Gen);
         return operations(operations'First+Natural(
                           Ada.Numerics.Float_Random.Random(Gen)*Float(operations'Last)));
      end Generate_Laziness;

   end Generator;

   task body Worker is
      JobListTask : JobList.Job_List_Pointer;
      MagazineTask: MagazinePackage.Magazine_Pointer;
      j : Jobs.Job;
      WorkerId: Integer;
      MachineRoomTask : machineroom.MachineRoom_Pointer;
      machineTask : machine.Machine_Pointer;
      lazy : Boolean;
      Service : ServicePackage.Service_Pointer;
      Id: Natural;
   begin
      accept Start (jobT : in JobList.Job_List_Pointer;magazineT : in MagazinePackage.Magazine_Pointer ; id: in Integer;machineT :in machineroom.MachineRoom_Pointer;s : in Service_Pointer) do
         JobListTask:=jobT;
         MagazineTask:=magazineT;
         WorkerId:=id;
         MachineRoomTask:=machineT;
         lazy := Generator.Generate_Laziness;
         Service:=s;

      end Start;
      MenuPackage.LazineesArray(WorkerId):=lazy;
      -- Put_Line(Integer'Image(WorkerId) & " is " & Boolean'Image(lazy));
      loop
         JobListTask.Retrive(j);
         if lazy=True then
            MachineRoomTask.GetMachine(j,machineTask,Id);
            loop
            machineTask.Insert(j);
            if (j.Result=0.0) then
               Service.HelpDesk(id        =>Id ,
                                operation =>j.O );
            else
                  MenuPackage.CountArray(WorkerId):=MenuPackage.CountArray(WorkerId)+1;
                  exit;
               end if;
               end loop;
         else
            loop
               MachineRoomTask.GetMachine(j,machineTask,Id);
               select
                  machineTask.Insert(j);
                  if(j.Result=0.0) then
                     Service.HelpDesk(id        =>Id ,
                                      operation =>j.O );
                  else
                     MenuPackage.CountArray(WorkerId):=MenuPackage.CountArray(WorkerId)+1;
                     exit;
                  end if;
               or
                  delay(variables.WorkerWait);
                  if variables.Is_Silent = false then
                     Put_Line("Changed Machine! Worker Number : "&Integer'Image(WorkerId));
                  end if;

               end select;
            end loop;
         end if;
         MagazineTask.Insert(j.Result);
         if variables.Is_Silent = false then
            Put_Line("Worker with id:" & Integer'Image(WorkerId) & " did job :" & Float'Image(j.X) & Jobs.Op'Image(j.O) & Float'Image(j.Y) & "with result " & Float'Image(j.Result) );
         end if;

         delay(variables.WorkerSleep);
      end loop;

   end Worker;
end WorkerPackage;
