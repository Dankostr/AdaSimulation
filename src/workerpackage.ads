with Jobs;use Jobs;
with JobList; Use JobList;
with MagazinePackage;use MagazinePackage;
with Ada.Text_IO;use Ada.Text_IO;
with variables;use variables;
with machine;use machine;
with machineroom;use machineroom;
with Ada.Numerics.Float_Random;use Ada.Numerics;
with MenuPackage;use MenuPackage;
with ServicePackage; use ServicePackage;

package WorkerPackage is
   task type Worker is
      entry Start(jobT: in JobList.Job_List_Pointer ; magazineT: in MagazinePackage.Magazine_Pointer; id: in Integer; machineT: in machineroom.MachineRoom_Pointer;s : in Service_Pointer);
   end Worker;
      type BooleanArr is array (Natural range <>) of Boolean;

    protected Generator is
      function Generate_Laziness return Boolean;
      private
      Gen : Ada.Numerics.Float_Random.Generator;

   end Generator;
end WorkerPackage;
